"""
interact async with the opendata api of the Belgian STIB MIVB public transport company
"""
__version__ = "0.9.3"

from .odstibmivb import *  # noqa: F401 F403
